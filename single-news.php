<?php
/*
 * Template Name: SINGLE NEWS
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

$url =  "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
$escaped_url = urlencode(htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' ));

$fb_url = 'https://www.facebook.com/sharer/sharer.php?u='.$escaped_url;

$cur_title = urlencode(get_the_title());
$linkedin_url = 'https://www.linkedin.com/shareArticle?mini=true&url='.$escaped_url.'&title='.$cur_title;

//$twitter_url = 'https://twitter.com/home?status='.$escaped_url;
//$twitter_params = urlencode(get_the_title() . " ". htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' ) . ' via @matchupeu');
//$twitter_url = 'https://twitter.com/home?status='.$twitter_params;
$twitter_url = 'https://twitter.com/intent/tweet?url='.$escaped_url.'&via=bambooH2020&text='.$cur_title;



$context['fb_url'] = $fb_url;
$context['linkedin_url'] = $linkedin_url;
$context['twitter_url'] = $twitter_url;

$context["is_single_news"] = true;

$context['news_archive_link'] = get_permalink(13);

Timber::render( 'single-news.twig', $context );
