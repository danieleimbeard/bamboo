<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php') ) . '</a></p></div>';
	});
	
	add_filter('template_include', function($template) {
		return get_stylesheet_directory() . '/static/no-timber.html';
	});
	
	return;
}

Timber::$dirname = array('templates', 'views');

class StarterSite extends TimberSite {

	function __construct() {
		add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );

		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_action( 'wp_head', 'feed_links', 2 );

		add_filter('show_admin_bar', '__return_false');


		add_filter( 'the_content', array( $this, 'surround_img_tags_with_div' ), 99 );
		add_filter( 'acf_the_content', array( $this, 'surround_img_tags_with_div' ), 99 );

		add_action( 'wp_ajax_load_news', array( $this, 'load_news' ) );
		add_action( 'wp_ajax_nopriv_load_news', array( $this, 'load_news' ) );

		add_action( 'wp_ajax_load_events_by_ajax', array( $this, 'load_events_by_ajax' ) );
		add_action( 'wp_ajax_nopriv_load_events_by_ajax', array( $this, 'load_events_by_ajax' ) );

		add_action( 'wp_ajax_load_technicals', array( $this, 'load_technicals' ) );
		add_action( 'wp_ajax_nopriv_load_technicals', array( $this, 'load_technicals' ) );

		add_action('admin_enqueue_scripts', array( $this, 'admin_style' ));

		if( function_exists('acf_add_options_page') ) {
			acf_add_options_page();
		}

		parent::__construct();
	}

	

	function register_post_types() {
		//this is where you can register custom post types
	}

	function register_taxonomies() {
		//this is where you can register custom taxonomies
	}

	// Aggiorno il CSS nell’area amministratore
	function admin_style() {
		wp_enqueue_style('admin-styles', get_template_directory_uri().'/admin-style.css');
	}

	function add_to_context( $context ) {
		$context['foo'] = 'bar';
		$context['stuff'] = 'I am a value set in your functions.php file';
		$context['notes'] = 'These values are available everytime you call Timber::get_context();';
		$context['options'] = get_fields('option');
		$context['menu'] = new TimberMenu();
		$context['site'] = $this;

		$context['events_archive_link'] = get_post_type_archive_link( 'tribe_events' );
		$max_featured = 2;
		$remaining_featured = 1;
		$context['single_news'] = false;
		$exclude_id = [];

		if ((is_singular( 'news' )) || (is_singular( 'tribe_events' ))){
			$exclude_id[0] = get_the_ID();
		}

		$featured_total = 0;
		$featured_type = '';

		$featured_articles = [];
		$featured_article_subtitles = [];
		$featured_article_cats = [];

		$args = array(
			'numberposts'	=> $remaining_featured,
			'post_type' => 'tribe_events',
	  		'post__not_in' => $exclude_id
		);

		$custom_query = new WP_Query($args);

		if ( $custom_query->have_posts() ){
		  while ( $custom_query->have_posts() ) :
				$custom_query->the_post();
	      		$cur_id = get_the_ID();
	      		$exclude_id[] = $cur_id;
				$featured_articles[] = new TimberPost($cur_id);
				$featured_article_subtitles[] = getsubtitle($cur_id, true);
				$featured_article_cats[] = getEventCategory($cur_id);
				//$featured_article_cats[] = get_field("upcoming_event_label","options");
				$featured_total++;
			endwhile;
		}


		$remaining_featured = $max_featured - $featured_total;

		/*if($remaining_featured == 0)
		{
			$remaining_featured = 1;
		}
		else
		{
			$remaining_featured = 2;
		}*/

		if($remaining_featured > 0){
			$args = array(
				'numberposts'	=> $remaining_featured,
				'post_type'		=> 'news',
			  'order_by'    => 'date',
			  'order'       => 'DESC',
			  'posts_per_page' => $remaining_featured,
			  'post__not_in' => $exclude_id
			);

			$custom_query = new WP_Query($args);

			if ( $custom_query->have_posts() ){
			  while ( $custom_query->have_posts() ) :
					$custom_query->the_post();
		      		$cur_id = get_the_ID();
		      		$exclude_id[] = $cur_id;
					$featured_articles[] = new TimberPost($cur_id);
					$featured_article_subtitles[] = getsubtitle($cur_id);
					$featured_article_cats[] = '';
					$featured_total++;
				endwhile;
			}
		}

		$featured_past_event = false;
		
		$remaining_featured = $max_featured - $featured_total;


		if($remaining_featured > 0){
			$featured_past_event = true;
			$args = array(
			'post_type' => 'tribe_events',
			'numberposts'	=> $remaining_featured,
			'post_status'=>'publish',
			'eventDisplay' => 'past',
				'meta_key'=>'_EventStartDate',
			  'orderby'=>'_EventStartDate',
			  'order'=>'DESC'

			);

			$custom_query = new WP_Query($args);

			if ( $custom_query->have_posts() ){
			  while ( $custom_query->have_posts() ) :
					$custom_query->the_post();
		      		$cur_id = get_the_ID();
		      		$exclude_id[] = $cur_id;
					$featured_articles[] = new TimberPost($cur_id);
					$featured_article_subtitles[] = getsubtitle($cur_id);
					$featured_article_cats[] = getEventCategory($cur_id);
					$featured_total++;
				endwhile;
			}
		}

		$context['featured_articles'] = $featured_articles;
		$context['featured_article_subtitles'] = $featured_article_subtitles;
		$context['featured_article_cats'] = $featured_article_cats;
		$context['featured_past_event'] = $featured_past_event;



		return $context;
	}

	function surround_img_tags_with_div( $content )
	{
			// A regular expression of what to look for.
		 $pattern = '/(<img([^>]*)>)/i';
		 // What to replace it with. $1 refers to the content in the first 'capture group', in parentheses above
		 $replacement = '<div class="image-container">$1</div>';

		 // run preg_replace() on the $content
		 $content = preg_replace( $pattern, $replacement, $content );

		 // return the processed content
		 return $content;
	}

	function myfoo( $text ) {
		$text .= ' bar!';
		return $text;
	}

	function add_to_twig( $twig ) {
		/* this is where you can add your own functions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		$twig->addFilter('myfoo', new Twig_SimpleFilter('myfoo', array($this, 'myfoo')));
		return $twig;
	}

	function use_excerpt_in_gcal_link( array $parameters ) {
		$parameters['details'] = urlencode( get_the_excerpt() );
		return $parameters;
	}



	function load_news($page, $category,$not_id) {

	    $msg = '';

	    if( isset( $_POST['data']['page'] ) ){
	        $page = sanitize_text_field($_POST['data']['page']);
	        $category = sanitize_text_field($_POST['data']['category']);
			$not_id = [];
			$not_id[] = sanitize_text_field($_POST['data']['not_id']);
			$cur_page = $page;
		    $per_page = 12;
		    
		    $start = $page * $per_page;
		    $no_nav = false;
			$orderby = "date";
			$order = 'DESC';

			$type = 'news';
			$args = [];

			if($category != 0)
			{
				$args = array(
					'post_type' => $type,
					'posts_per_page' => $per_page,
					'paged'          => $cur_page,
					'post_status'=>'publish',
					'orderby' => $orderby,
					'order' => $order,
					'post__not_in' => $not_id,
					 'tax_query'      => array(
						 array(
								 'taxonomy'  => 'category',
								 'field'     => 'term_id',
								 'terms'     => $category
						 )
				 ),
				);
			}
			else {
				$args = array(
					'post_type' => $type,
					'posts_per_page' => $per_page,
					'paged'          => $cur_page,
					'post_status'=>'publish',
					'orderby' => $orderby,
					'order' => $order,
					'post__not_in' => $not_id
				);
			}

			$custom_query = new WP_Query($args);
			 $total = $custom_query->found_posts;
			$total_pages = $custom_query->max_num_pages;

		    if ( $custom_query->have_posts() ){

			 //$msg .= '<div id="grid" class=" row">';

			  while ( $custom_query->have_posts() ) :
			      $custom_query->the_post();
			      $size = 'large';
			      $cur_id = get_the_ID();

			      $post_categories = wp_get_post_categories( $cur_id );
			      $msg .= '<div class="col-xl-3 col-lg-4 col-md-6 col-preview">';
			      $msg .= '<a class="article-preview-medium" href="'.get_the_permalink($cur_id ).'">';
			      $msg .= '<div class="image-wrapper ">';
			      $msg .= '<div class="image-container imgLiquid imgLiquidFill"  data-imgLiquid-fill="true" data-imgLiquid-horizontalAlign="center" data-imgLiquid-verticalAlign="50%">';
			      $msg .= '<img  src="'.get_the_post_thumbnail_url($cur_id,'large').'" />';
			      $msg .= '</div><!-- image-container -->';
			      $msg .= '</div><!-- image-wrapper -->';
			      $msg .= '<h3 class="standard-title">'.get_the_title($cur_id).'</h3>';
			      $msg .= '<p><b>';

			       $counter = 0;
			       foreach($post_categories as $c){

			          if($counter != 0){
			            $msg .= ", ";
			          }

			          $cat = get_category( $c );
			          //$cats[] = array( 'name' => $cat->name, 'slug' => $cat->slug );
			          $msg .= $cat->description;

			          $counter++;
			        }

			      $msg .= " - ".get_the_date( 'd M Y', $cur_id );
			      $msg .= '</b><br/>';
			      $msg .= get_the_excerpt( $cur_id );
			      $msg .= '</p>';
			      $msg .= '<span  class="preview-link">';
			      $msg .= get_field("read_more_label","options");
			      $msg .= '</span>';
			      $msg .= '</a><!-- article-preview -->';
			      $msg .= '</div><!-- col-preview -->';

			  endwhile;

			  //$msg .= '</div><!-- row -->';

			$debug  = '';
				  $page = get_field("page_label","options");
			    $first = get_field("first_label","options");
			    $previous = get_field("prev_label","options");
			    $next = get_field("next_label","options");
			    $last = get_field("last_label","options");
			    $of = get_field("of_label","options");

				  if(($total >= $per_page) && ($total_pages > 1))
				  {
				      $msg .= '<div class="pagination text-center ">';

				      $msg .= '<span class="d-block d-md-none">'.$page.' '.$cur_page.' of '.$total_pages.'</span>';
				      $class = '';

				      if($cur_page == 1)
				        $class = 'disabled';

				      $msg .= '<a class="'.$class.'" href="#" dat-paged="1" >'.$first.'</a>';
				      $msg .= '<a class="'.$class.'" href="#" data-paged="'.($cur_page - 1).'" >←'.$previous.'</a>';
				      $msg .= '<span class="d-none d-md-block d-lg-block">'.$page.' '.$cur_page.' '.$of.' '.$total_pages.'</span>';
				      $class = '';

				      if($cur_page == $total_pages)
				        $class = 'disabled';

				      $msg .= '<a class="'.$class.'" href="#" data-paged="'.($cur_page + 1).'" >'.$next.'→</a>';
				      $msg .= '<a class="'.$class.'" href="#" data-paged="'.$total_pages.'" >'.$last .'</a>';
				      $msg .= '</div><!-- pagination --> ';
				  }
				}

				echo $msg;
			}

	    exit();

	}

	function load_events_by_ajax($page, $year) {
			//write_log("mi ha chiamato");
	    $msg = '';

	    if( isset( $_POST['data']['page'] ) ){
	        $page = sanitize_text_field($_POST['data']['page']);

			$cur_page = $page;
			$per_page = 12; // POI 12
		    $start = $page * $per_page;
			$orderby = "date";
			$order = 'DESC';

			$type = 'tribe_events';
			$args = [];

			$args = array(
				'post_type' => 'tribe_events',
				'posts_per_page' => $per_page,
				'paged'          => $cur_page,
				'post_status'=>'publish',
				'eventDisplay' => 'past',
				'meta_key'=>'_EventStartDate',
			  'orderby'=>'_EventStartDate',
			  'order'=>'DESC'

			);

			$custom_query = new WP_Query($args);
			 $total = $custom_query->found_posts;
			$total_pages = $custom_query->max_num_pages;

		    if ( $custom_query->have_posts() ){

					//$msg .= '<div class="past-events col-list">';
					//$msg .= '<div class="row">';

			  while ( $custom_query->have_posts() ){
						$custom_query->the_post();
			      //setup_postdata( $post );
			      $size = 'medium';
			      $cur_id = get_the_ID();

			    $msg .= '<div class="col-xl-3 col-lg-4 col-md-6 col-preview">';
			      $msg .= '<a class="article-preview-medium" href="'.get_the_permalink($cur_id ).'">';
			      $msg .= '<div class="image-wrapper ">';
				  $msg .= '<div class="image-container imgLiquid imgLiquidFill"  data-imgLiquid-fill="true" data-imgLiquid-horizontalAlign="center" data-imgLiquid-verticalAlign="50%">';
			      $msg .= '<img  src="'.get_the_post_thumbnail_url($cur_id,'large').'" />';
			      $msg .= '</div><!-- image-container -->';
			      $msg .= '</div><!-- image-wrapper -->';
			      $msg .= '<h3 class="standard-title">'.get_the_title($cur_id).'</h3>';
			       $msg .= '<p>';
			      $msg .= '<b>';

				  $event = get_post( $cur_id);

					$msg .= getEventVenuePreview($cur_id).' - '.getEventDatePreview($event);
				   $msg .= '</b><br/>';


			      $msg .= get_the_excerpt( $cur_id );
			      $msg .= '</p>';
			       $msg .= '<span  class="preview-link ">';
			      $msg .= get_field("learn_more_label","options");
			      $msg .= '</span>';
			      $msg .= '</a><!-- article-preview -->';
			      $msg .= '</div><!-- col-preview -->';
			  }

			 // $msg .= '</div><!-- row -->';

				$page = get_field("page_label","options");
			    $first = get_field("first_label","options");
			    $previous = get_field("prev_label","options");
			    $next = get_field("next_label","options");
			    $last = get_field("last_label","options");
			    $of = get_field("of_label","options");

				  if(($total >= $per_page) && ($total_pages > 1))
				  {
				      $msg .= '<div class="pagination text-center ">';

				      $msg .= '<span class="d-block d-md-none">'.$page.' '.$cur_page.' of '.$total_pages.'</span>';
				      $class = '';

				      if($cur_page == 1)
				        $class = 'disabled';

				      $msg .= '<a class="'.$class.'" href="#" dat-paged="1" >'.$first.'</a>';
				      $msg .= '<a class="'.$class.'" href="#" data-paged="'.($cur_page - 1).'" >←'.$previous.'</a>';
				      $msg .= '<span class="d-none d-md-block d-lg-block">'.$page.' '.$cur_page.' '.$of.' '.$total_pages.'</span>';
				      $class = '';

				      if($cur_page == $total_pages)
				        $class = 'disabled';

				      $msg .= '<a class="'.$class.'" href="#" data-paged="'.($cur_page + 1).'" >'.$next.'→</a>';
				      $msg .= '<a class="'.$class.'" href="#" data-paged="'.$total_pages.'" >'.$last .'</a>';
				      $msg .= '</div><!-- pagination --> ';
				  }


				//$msg .= '</div><!-- past-events --> ';
			}




			echo $msg;
		}

	  exit();

	}

	function load_technicals($page = null, $category = null) {
			write_log("mi ha chiamato");
	    $msg = '';

	    if( isset( $_POST['data']['page'] ) ){
	        $page = sanitize_text_field($_POST['data']['page']);
	        $category = sanitize_text_field($_POST['data']['category']);

			$cur_page = $page;
		    $per_page = 4;
		    $previous_btn = true;
		    $next_btn = true;
		    $first_btn = true;
		    $last_btn = true;
		    $start = $page * $per_page;
		    $no_nav = false;
			$orderby = "date";
			$order = 'DESC';

			$type = 'resources';
			$args = [];

			if($category != 0)
			{
				$args = array(
					'post_type' => $type,
					'posts_per_page' => $per_page,
					'paged'          => $cur_page,
					'post_status'=>'publish',
					'orderby' => $orderby,
					'order' => $order,
					 'tax_query'      => array(
						 array(
								 'taxonomy'  => 'resources_category',
								 'field'     => 'term_id',
								 'terms'     => $category
						 )
				 ),
				);
			}
			else {
				$args = array(
					'post_type' => $type,
					'posts_per_page' => $per_page,
					'paged'          => $cur_page,
					'post_status'=>'publish',
					'orderby' => $orderby,
					'order' => $order
				);
			}

			$custom_query = new WP_Query($args);
			$total = $custom_query->found_posts;
			$total_pages = $custom_query->max_num_pages;

			$years = [];
			global $wpdb;
			$query = $wpdb->prepare('
			            SELECT YEAR(%1$s.post_date) AS `year`, count(%1$s.ID) as `posts`
			            FROM %1$s
			            WHERE %1$s.post_type IN ("resources")
			            AND %1$s.post_status IN ("publish")
			            GROUP BY YEAR(%1$s.post_date)
			            ORDER BY %1$s.post_date',
			            $wpdb->posts
			        );
			$results = $wpdb->get_results($query);
			$years = $results;

			write_log("arriva qui");
		    if ( $custom_query->have_posts() ){
				$counter = 0;

				while ( $custom_query->have_posts() ) :


				     $custom_query->the_post();

				      $cur_id = get_the_ID();
				      $resource_year = get_the_date( 'Y', $cur_id );

				      $counter = 0;
				      $index = 0;
				      $found = false;

				      foreach($years as $year){
				        if($year->year == $resource_year){
				          $index = $counter;
				          $found = true;
				        }

				        $counter++;
				      }

				      if($found){
				        $msg .= '<h2 class="standard-title">';
				        $msg .= $resource_year;
				        $msg .= '</h2>';
				        unset($years[$index]);
				        array_values($years);
				      }

				      
				     $msg .= '<div class="technical-item">';

				     $msg .= '<div class="col-content">';
				      $msg .= '<h3 class="standard-title">'.get_the_title($cur_id).'</h3>';
				      $msg .= '<span class="category">';
				      $term_list = wp_get_post_terms($cur_id, 'resources_category', array("fields" => "all"));
				      
				      

				      if(!is_wp_error($term_list))
				      {
				        $msg .= $term_list[0]->description.' - ';
				      }

				      $msg .=  get_the_date( 'd M Y', $cur_id ); 

				      $msg .= '</span>';
				      $msg .= '<p>'.get_the_content($cur_id).'</p>';
				      $attachment_id = get_field('file_download',$cur_id);
				       $url = wp_get_attachment_url( $attachment_id['id'] );
				       $title = get_the_title( $cur_id );

				       $msg .= '<div class="list-links alternate">';
				       $msg .= '<a href="'.$attachment_id['url'].'" class="" target="_blank">';
				       $msg .= '<?xml version="1.0" encoding="UTF-8"?>
				                  <svg width="22px" height="24px" viewBox="0 0 22 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
				                      <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
				                      <title>freccia-3px</title>
				                      <desc>Created with Sketch.</desc>
				                      <defs></defs>
				                      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
				                          <g id="Artboard" transform="translate(-26.000000, -50.000000)" stroke="#00AB84" stroke-width="3">
				                              <g id="freccia-3px" transform="translate(26.000000, 52.000000)">
				                                  <polyline id="Stroke-1" points="9 0 19.7116732 10 9 20"></polyline>
				                                  <path d="M20,10 L0,10" id="Stroke-3"></path>
				                              </g>
				                          </g>
				                      </g>
				                  </svg>';
				      $msg .= '<span>';
				      $msg .= get_field("download_label", "options");
				      $msg .= '</span>';
				       $msg .= '</a>';
				       $msg .= '</div><!-- list-links -->';
				     $msg .= '</div><!-- col-content -->';
				     $msg .= '</div><!-- resource-item -->';
				     $counter++;
					  endwhile;

					   $page = get_field("page_label","options");
					    $first = get_field("first_label","options");
					    $previous = get_field("prev_label","options");
					    $next = get_field("next_label","options");
					    $last = get_field("last_label","options");
					    $of = get_field("of_label","options");

					  if(($total >= $per_page) && ($total_pages > 1))
					  {
					      $msg .= '<div class="pagination text-center ">';

					      $msg .= '<span class="d-block d-md-none">'.$page.' '.$cur_page.' of '.$total_pages.'</span>';
					      $class = '';

					      if($cur_page == 1)
					        $class = 'disabled';

					      $msg .= '<a class="'.$class.'" href="#" dat-paged="1" >'.$first.'</a>';
					      $msg .= '<a class="'.$class.'" href="#" data-paged="'.($cur_page - 1).'" >←'.$previous.'</a>';
					      $msg .= '<span class="d-none d-md-block d-lg-block">'.$page.' '.$cur_page.' '.$of.' '.$total_pages.'</span>';
					      $class = '';

					      if($cur_page == $total_pages)
					        $class = 'disabled';

					      $msg .= '<a class="'.$class.'" href="#" data-paged="'.($cur_page + 1).'" >'.$next.'→</a>';
					      $msg .= '<a class="'.$class.'" href="#" data-paged="'.$total_pages.'" >'.$last .'</a>';
					      $msg .= '</div><!-- pagination --> ';
					  }
					}




					echo $msg;
			}

	    exit();

	}
}

new StarterSite();

function ft_remove_custom_menus () {
    
	//Remove menus by not allowed user IDs
    $allowed_ids = array(1);
    if( !in_array(get_current_user_id(), $allowed_ids) ) {
    	
    	remove_menu_page( 'index.php' );
    	remove_menu_page( 'users.php' );
    	remove_menu_page( 'edit.php?post_type=acf-field-group' );
    	remove_menu_page( 'tools.php' );
    	remove_menu_page( 'options-general.php' );
    	remove_menu_page( 'theme-editor.php' );
    	remove_submenu_page( 'themes.php', 'theme-editor.php' );
    	remove_submenu_page( 'themes.php', 'themes.php' );
    	remove_submenu_page( 'plugins.php', 'plugin-install.php' );
    	remove_submenu_page( 'plugins.php', 'plugin-editor.php' );
    	remove_menu_page( 'update-core.php' );
    	remove_menu_page( 'edit-comments.php' );

    	add_filter( 'pre_site_transient_update_core', 'disable_updates' );     // Disable WordPress core updates
        add_filter( 'pre_site_transient_update_themes', 'disable_updates' );
    }    
}

add_action( 'admin_menu', 'ft_remove_custom_menus', 999 );

function mytheme_admin_bar_render() {
    global $wp_admin_bar;

    $wp_admin_bar->remove_menu('comments');
    $wp_admin_bar->remove_menu('updates');
    $wp_admin_bar->remove_menu('new-content');

}
// and we hook our function via
add_action( 'wp_before_admin_bar_render', 'mytheme_admin_bar_render' );


function getEventDatePreview($event){

	$start_date_no_time = tribe_get_start_date($event,false,'dmY');
	$end_date_no_time = tribe_get_end_date($event,false,'dmY');
	$start_day = tribe_get_start_date($event,false,'j');
	$start_month = tribe_get_start_date($event,false,'M');
	$start_year = tribe_get_start_date($event,false,'Y');
	$end_day = tribe_get_end_date($event,false,'j');
	$end_month = tribe_get_end_date($event,false,'M');
	$end_year = tribe_get_end_date($event,false,'Y');

	$the_string = '';

	if($start_date_no_time == $end_date_no_time){
		//$the_string .= $start_date->format('j M y');
		$the_string .= tribe_get_start_date($event,false,'j M Y');
	}
	else
	{
		if($start_month == $end_month){
			//$start_day = $start_date->format('j');
			//$the_string .= $start_day .'-'.$end_date->format('j M y');
			$the_string .= $start_day .'-'.tribe_get_end_date($event,false,'j M Y');
		}
		else
		{
			$start_day = tribe_get_start_date($event,false,'j M');;
			$the_string .= $start_day .'-'.tribe_get_end_date($event,false,'j M Y');
		}
	}

	return $the_string;
}

function getEventVenuePreview($event_id){
	$venue_string = "";

	$venue = get_field ("venue",$event_id);
	$address = get_field ("address",$event_id);
	$city = get_field ("city",$event_id);
	$country = get_field ("country",$event_id);

	if($venue || $city  || $country){
		$venue_string = '';

		if($city && $country){
			$venue_string = $city.", ".$country;
		}
		else
		if($city){
			$venue_string = $city;
		}
		else
		if($country){
			$venue_string = $country;
		}
		else
		if($venue){
			$venue_string = $venue;
		}
	}
	else{
		$city = tribe_get_city ($event_id);
		$country = tribe_get_country ($event_id);

		if($city || $country){
			$venue_string = $city .", ".$country;
		}
	}

	return $venue_string;
}


function getFacebookShareUrl($url,$title){
	$escaped_url = urlencode(htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' ));
	$share_string = 'https://www.facebook.com/sharer/sharer.php?u='.$escaped_url;
	return $share_string;
}

function getLinkdnShareUrl($url,$title){
	$escaped_url = urlencode(htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' ));
	$cur_title = urlencode($title);
	$share_string = 'https://www.linkedin.com/shareArticle?mini=true&url='.$escaped_url.'&title='.$cur_title;
	return $share_string;
}

function getTwitterShareUrl($url,$title){

	$twitter_params = urlencode($title . " ". htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' ) . ' via @matchupeu');
	$share_string = 'https://twitter.com/home?status='.$twitter_params;
	return $share_string;
}

function getsubtitle($id,$upcoming = false){
	$message = '';
	//$GLOBALS['featured_type'] = get_post_type($id);
	$featured_type = get_post_type($id);

	if($featured_type == "news")
	{
		$post_categories = wp_get_post_categories( $id );
		$counter = 0;

		foreach($post_categories as $c){

			if($counter != 0){
				$message .= ", ";
			}

			$cat = get_category( $c );
			//$cats[] = array( 'name' => $cat->name, 'slug' => $cat->slug );
			$message .= $cat->description;

			$counter++;
		}

		$message .= ' - '.get_the_date( 'd M Y', $id);
	}
	else
	if($featured_type == "tribe_events")
	{
		$event = get_post($id);

		if(!$upcoming)
			$message = getEventVenuePreview($id).' - '.getEventDatePreview($event);
		else
			$message = get_field("upcoming_event_label","options").' - '.getEventDatePreview($event);
	}

	return $message;
}

function getEventCategory($id){
	$message = '';

	$term_list = wp_get_post_terms( $id, 'tribe_events_cat' );
 
    foreach( $term_list as $term_single ) {
        $message .= $term_single->description . ' - ';
    }

	return $message;
}

function tribe_is_a_past_event( $event = null ){
	if ( ! tribe_is_event( $event ) ){
		return false;
	}
	$event = tribe_events_get_event( $event );
	// Grab the event End Date as UNIX time
	$end_date = tribe_get_end_date( $event, true, 'U' );
	
	return time() > $end_date;
}

function write_log ( $log )  {
	if ( true === WP_DEBUG ) {
		if ( is_array( $log ) || is_object( $log ) ) {
				error_log( print_r( $log, true ) );
		} else {
				error_log( $log );
		}
	}
}