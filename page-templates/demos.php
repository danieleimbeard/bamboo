<?php
/*
 * Template Name: DEMOS
 */


$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;


$args = array(
  'numberposts' => -1,
  'post_type'   => 'demos',
  'order_by'    => 'id',
  'order'       => 'ASC'
);

$context['demos'] = Timber::get_posts( $args );

$context['adminurl'] = admin_url('admin-ajax.php');

Timber::render( 'demos.twig', $context );
