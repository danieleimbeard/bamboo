<?php
/*
 * Template Name: NEW
 *
 */



$context = Timber::get_context();

$post = new TimberPost();
$context['post'] = $post;

$context['news_category'] = Timber::get_terms( 'category', array(
    'hide_empty' => true,
) );


$args = array(
  'numberposts' => 1,
  'post_type'   => 'news',
  'order_by'    => 'date',
  'order'       => 'DESC',
  'posts_per_page' => 1,
);

$first_id = [];
$first_query = new WP_Query($args);



if ( $first_query->have_posts() ){
  while ( $first_query->have_posts() ) :
    $first_query->the_post();
    $first_id[0] = get_the_ID();

  endwhile;
}

$first = Timber::get_posts( $args );
$context['first'] = $first;

$msg = '';
$cur_page = 1;
$per_page = 12; // poi 12

$args = array(
	'numberposts'	=> -1,
	'post_type'		=> 'news',
  'order_by'    => 'date',
  'order'       => 'DESC',
  'posts_per_page' => $per_page,
  'paged'          => $cur_page,
  'post__not_in' => $first_id
);

$custom_query = new WP_Query($args);
$total = $custom_query->found_posts;

$context['total'] = $total;

$total_pages = $custom_query->max_num_pages;

if ( $custom_query->have_posts() ){

 //$msg .= '<div id="grid" class=" row">';

  while ( $custom_query->have_posts() ) :
      $custom_query->the_post();
      $size = 'large';
      $cur_id = get_the_ID();

      $post_categories = wp_get_post_categories( $cur_id );
      $msg .= '<div class="col-xl-3 col-lg-4 col-md-6 col-preview">';
      $msg .= '<a class="article-preview-medium" href="'.get_the_permalink($cur_id ).'">';
      $msg .= '<div class="image-wrapper ">';
      $msg .= '<div class="image-container imgLiquid imgLiquidFill"  data-imgLiquid-fill="true" data-imgLiquid-horizontalAlign="center" data-imgLiquid-verticalAlign="50%">';
      $msg .= '<img  src="'.get_the_post_thumbnail_url($cur_id,'large').'" />';
      $msg .= '</div><!-- image-container -->';
      $msg .= '</div><!-- image-wrapper -->';
      $msg .= '<h3 class="standard-title">'.get_the_title($cur_id).'</h3>';
      $msg .= '<p><b>';

       $counter = 0;
       foreach($post_categories as $c){

          if($counter != 0){
            $msg .= ", ";
          }

          $cat = get_category( $c );
          //$cats[] = array( 'name' => $cat->name, 'slug' => $cat->slug );
          $msg .= $cat->description;

          $counter++;
        }

      $msg .= " - ".get_the_date( 'd M Y', $cur_id );
      $msg .= '</b><br/>';
      $msg .= get_the_excerpt( $cur_id );
      $msg .= '</p>';
      $msg .= '<span  class="preview-link">';
      $msg .= get_field("read_more_label","options");
      $msg .= '</span>';
      $msg .= '</a><!-- article-preview -->';
      $msg .= '</div><!-- col-preview -->';

  endwhile;

  //$msg .= '</div><!-- row -->';

$debug  = '';

  $context['total_pages'] = $total_pages;

    $page = get_field("page_label","options");
    $first = get_field("first_label","options");
    $previous = get_field("prev_label","options");
    $next = get_field("next_label","options");
    $last = get_field("last_label","options");
    $of = get_field("of_label","options");

  if(($total >= $per_page) && ($total_pages > 1))
  {
      $msg .= '<div class="pagination text-center ">';

      $msg .= '<span class="d-block d-md-none">'.$page.' '.$cur_page.' of '.$total_pages.'</span>';
      $class = '';

      if($cur_page == 1)
        $class = 'disabled';

      $msg .= '<a class="'.$class.'" href="#" dat-paged="1" >'.$first.'</a>';
      $msg .= '<a class="'.$class.'" href="#" data-paged="'.($cur_page - 1).'" >←'.$previous.'</a>';
      $msg .= '<span class="d-none d-md-block d-lg-block">'.$page.' '.$cur_page.' '.$of.' '.$total_pages.'</span>';
      $class = '';

      if($cur_page == $total_pages)
        $class = 'disabled';

      $msg .= '<a class="'.$class.'" href="#" data-paged="'.($cur_page + 1).'" >'.$next.'→</a>';
      $msg .= '<a class="'.$class.'" href="#" data-paged="'.$total_pages.'" >'.$last .'</a>';
      $msg .= '</div><!-- pagination --> ';
  }
}

$context['load_news_from_php_temporary'] = $msg;
$context['adminurl'] = admin_url('admin-ajax.php');


$context['is_news_archive'] = true;
$context['posts'] = new Timber\PostQuery();



$context['template'] = basename( get_page_template() );

Timber::render( 'archive-news.twig', $context );
