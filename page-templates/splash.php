<?php
/*
 * Template Name: SPLASH
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

$context['events'] = tribe_get_events( array(
 'posts_per_page' => 3,
 'start_date' => date( 'Y-m-d H:i:s' )
 ) );

Timber::render( 'splash.twig', $context );
