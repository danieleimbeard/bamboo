<?php
/*
 * Template Name: STANDARD
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
Timber::render( 'standard.twig', $context );
