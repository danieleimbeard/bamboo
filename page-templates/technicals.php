<?php
/*
 * Template Name: TECHNICALS
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

$context['categories'] = Timber::get_terms( 'resources_category', array(
    'hide_empty' => true,
) );

$msg = '';

$cur_page = 1;
$per_page = 12;

$args = array(
	'numberposts'	=> -1,
	'post_type'		=> 'resources',
  'order_by'    => 'date',
  'order'       => 'DESC',
  'posts_per_page' => $per_page,
  'paged'          => $cur_page
);

$custom_query = new WP_Query($args);

$years = [];

$query = $wpdb->prepare('
            SELECT YEAR(%1$s.post_date) AS `year`, count(%1$s.ID) as `posts`
            FROM %1$s
            WHERE %1$s.post_type IN ("resources")
            AND %1$s.post_status IN ("publish")
            GROUP BY YEAR(%1$s.post_date)
            ORDER BY %1$s.post_date',
            $wpdb->posts
        );
$results = $wpdb->get_results($query);
$years = $results;


$total = $custom_query->found_posts;
$total_pages = $custom_query->max_num_pages;


if ( $custom_query->have_posts() ){
  $counter = 0;

  while ( $custom_query->have_posts() ) :

      $custom_query->the_post();

      $cur_id = get_the_ID();
      $resource_year = get_the_date( 'Y', $cur_id );

      $counter = 0;
      $index = 0;
      $found = false;

      foreach($years as $year){
        if($year->year == $resource_year){
          $index = $counter;
          $found = true;
        }

        $counter++;
      }

      if($found){
        $msg .= '<h2 class="standard-title">';
        $msg .= $resource_year;
        $msg .= '</h2>';
        unset($years[$index]);
        array_values($years);
      }

      
     $msg .= '<div class="technical-item">';

     $msg .= '<div class="col-content">';
      $msg .= '<h3 class="standard-title">'.get_the_title($cur_id).'</h3>';
      $msg .= '<span class="category">';
      $term_list = wp_get_post_terms($cur_id, 'resources_category', array("fields" => "all"));
      
      

      if(!is_wp_error($term_list))
      {
        $msg .= $term_list[0]->description.' - ';
      }

      $msg .=  get_the_date( 'd M Y', $cur_id ); 

      $msg .= '</span>';
      $msg .= '<p>'.get_the_content($cur_id).'</p>';
      $attachment_id = get_field('file_download',$cur_id);
       $url = wp_get_attachment_url( $attachment_id['id'] );
       $title = get_the_title( $cur_id );

       $msg .= '<div class="list-links alternate">';
       $msg .= '<a href="'.$attachment_id['url'].'" class="" target="_blank">';
       $msg .= '<?xml version="1.0" encoding="UTF-8"?>
                  <svg width="22px" height="24px" viewBox="0 0 22 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                      <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                      <title>freccia-3px</title>
                      <desc>Created with Sketch.</desc>
                      <defs></defs>
                      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <g id="Artboard" transform="translate(-26.000000, -50.000000)" stroke="#00AB84" stroke-width="3">
                              <g id="freccia-3px" transform="translate(26.000000, 52.000000)">
                                  <polyline id="Stroke-1" points="9 0 19.7116732 10 9 20"></polyline>
                                  <path d="M20,10 L0,10" id="Stroke-3"></path>
                              </g>
                          </g>
                      </g>
                  </svg>';
      $msg .= '<span>';
      $msg .= get_field("download_label", "options");
      $msg .= '</span>';
       $msg .= '</a>';
       $msg .= '</div><!-- list-links -->';
     $msg .= '</div><!-- col-content -->';
     $msg .= '</div><!-- resource-item -->';
     $counter++;
  endwhile;

$debug  = '';

  $context['total_pages'] = $total_pages;

    $page = get_field("page_label","options");
    $first = get_field("first_label","options");
    $previous = get_field("prev_label","options");
    $next = get_field("next_label","options");
    $last = get_field("last_label","options");
    $of = get_field("of_label","options");

  if(($total >= $per_page) && ($total_pages > 1))
  {
      $msg .= '<div class="pagination text-center ">';

      $msg .= '<span class="d-block d-md-none">'.$page.' '.$cur_page.' of '.$total_pages.'</span>';
      $class = '';

      if($cur_page == 1)
        $class = 'disabled';

      $msg .= '<a class="'.$class.'" href="#" dat-paged="1" >'.$first.'</a>';
      $msg .= '<a class="'.$class.'" href="#" data-paged="'.($cur_page - 1).'" >←'.$previous.'</a>';
      $msg .= '<span class="d-none d-md-block d-lg-block">'.$page.' '.$cur_page.' '.$of.' '.$total_pages.'</span>';
      $class = '';

      if($cur_page == $total_pages)
        $class = 'disabled';

      $msg .= '<a class="'.$class.'" href="#" data-paged="'.($cur_page + 1).'" >'.$next.'→</a>';
      $msg .= '<a class="'.$class.'" href="#" data-paged="'.$total_pages.'" >'.$last .'</a>';
      $msg .= '</div><!-- pagination --> ';
  }

}

$context['load_technicals_from_php_temporary'] = $msg;
$context['adminurl'] = admin_url('admin-ajax.php');



$context['total'] = $total;

Timber::render( 'technicals.twig', $context );
