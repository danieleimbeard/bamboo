<?php
/*
 * Template Name: ABOUT
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$context['template'] = basename( get_page_template() );

$args = array(
	'numberposts'	=> -1,
	'post_type'		=> 'partners',
  'order_by'    => 'date',
  'order'       => 'ASC'
);

$context['partners'] = Timber::get_posts( $args );

$args = array(
	'numberposts'	=> -1,
	'post_type'		=> 'sister_projects',
  'order_by'    => 'date',
  'order'       => 'ASC'
);

$context['sister_projects'] = Timber::get_posts( $args );

Timber::render( 'about.twig', $context );
