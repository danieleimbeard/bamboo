<?php
/*
 * Template Name: HOMEPAGE
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$context['is_front_page'] = 'true';

Timber::render( 'homepage.twig', $context );
