<?php
/*
 * Template Name: RESOURCES
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

$args = array(
	'numberposts'	=> -1,
	'post_type'		=> 'resources',
  'order_by'    => 'date',
  'order'       => 'DESC',
  'posts_per_page' => $per_page,
  'paged'          => $cur_page
);

$context['resources'] = Timber::get_posts( $args );

Timber::render( 'resources.twig', $context );
