<?php
/*
 * Template Name: NEWSLETTER FORM
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

$context['newsletter_form'] = true;

Timber::render( 'newsletter-form.twig', $context );
