// (tweens at bottom)


/**
 * BezierEasing - use bezier curve for transition easing function
 * is based on Firefox's nsSMILKeySpline.cpp
 * Usage:
 * var spline = BezierEasing(0.25, 0.1, 0.25, 1.0)
 * spline(x) => returns the easing value | x must be in [0, 1] range
 *
 * bezier-easing 0.4.0
 * BSD License
 * Gaëtan Renaudeau
 * https://github.com/gre/bezier-easing
 */
(function (definition) {
  if (typeof exports === "object") {
    module.exports = definition();
  }
  else if (typeof window.define === 'function' && window.define.amd) {
    window.define([], definition);
  } else {
    window.BezierEasing = definition();
  }
}(function () {

  // These values are established by empiricism with tests (tradeoff: performance VS precision)
  var NEWTON_ITERATIONS = 4;
  var NEWTON_MIN_SLOPE = 0.001;
  var SUBDIVISION_PRECISION = 0.0000001;
  var SUBDIVISION_MAX_ITERATIONS = 10;

  var kSplineTableSize = 11;
  var kSampleStepSize = 1.0 / (kSplineTableSize - 1.0);

  var float32ArraySupported = typeof Float32Array === "function";

  function BezierEasing (mX1, mY1, mX2, mY2) {
    // Validate arguments
    if (arguments.length !== 4) {
      throw new Error("BezierEasing requires 4 arguments.");
    }
    for (var i=0; i<4; ++i) {
      if (typeof arguments[i] !== "number" || isNaN(arguments[i]) || !isFinite(arguments[i])) {
        throw new Error("BezierEasing arguments should be integers.");
      } 
    }
    if (mX1 < 0 || mX1 > 1 || mX2 < 0 || mX2 > 1) {
      throw new Error("BezierEasing x values must be in [0, 1] range.");
    }

    var mSampleValues = float32ArraySupported ? new Float32Array(kSplineTableSize) : new Array(kSplineTableSize);
   
    function A (aA1, aA2) { return 1.0 - 3.0 * aA2 + 3.0 * aA1; }
    function B (aA1, aA2) { return 3.0 * aA2 - 6.0 * aA1; }
    function C (aA1)      { return 3.0 * aA1; }
   
    // Returns x(t) given t, x1, and x2, or y(t) given t, y1, and y2.
    function calcBezier (aT, aA1, aA2) {
      return ((A(aA1, aA2)*aT + B(aA1, aA2))*aT + C(aA1))*aT;
    }
   
    // Returns dx/dt given t, x1, and x2, or dy/dt given t, y1, and y2.
    function getSlope (aT, aA1, aA2) {
      return 3.0 * A(aA1, aA2)*aT*aT + 2.0 * B(aA1, aA2) * aT + C(aA1);
    }

    function newtonRaphsonIterate (aX, aGuessT) {
      for (var i = 0; i < NEWTON_ITERATIONS; ++i) {
        var currentSlope = getSlope(aGuessT, mX1, mX2);
        if (currentSlope === 0.0) return aGuessT;
        var currentX = calcBezier(aGuessT, mX1, mX2) - aX;
        aGuessT -= currentX / currentSlope;
      }
      return aGuessT;
    }

    function calcSampleValues () {
      for (var i = 0; i < kSplineTableSize; ++i) {
        mSampleValues[i] = calcBezier(i * kSampleStepSize, mX1, mX2);
      }
    }

    function binarySubdivide (aX, aA, aB) {
      var currentX, currentT, i = 0;
      do {
        currentT = aA + (aB - aA) / 2.0;
        currentX = calcBezier(currentT, mX1, mX2) - aX;
        if (currentX > 0.0) {
          aB = currentT;
        } else {
          aA = currentT;
        }
      } while (Math.abs(currentX) > SUBDIVISION_PRECISION && ++i < SUBDIVISION_MAX_ITERATIONS);
      return currentT;
    }

    function getTForX (aX) {
      var intervalStart = 0.0;
      var currentSample = 1;
      var lastSample = kSplineTableSize - 1;

      for (; currentSample != lastSample && mSampleValues[currentSample] <= aX; ++currentSample) {
        intervalStart += kSampleStepSize;
      }
      --currentSample;

      // Interpolate to provide an initial guess for t
      var dist = (aX - mSampleValues[currentSample]) / (mSampleValues[currentSample+1] - mSampleValues[currentSample]);
      var guessForT = intervalStart + dist * kSampleStepSize;

      var initialSlope = getSlope(guessForT, mX1, mX2);
      if (initialSlope >= NEWTON_MIN_SLOPE) {
        return newtonRaphsonIterate(aX, guessForT);
      } else if (initialSlope == 0.0) {
        return guessForT;
      } else {
        return binarySubdivide(aX, intervalStart, intervalStart + kSampleStepSize);
      }
    }

    if (mX1 != mY1 || mX2 != mY2)
      calcSampleValues();

    var f = function (aX) {
      if (mX1 === mY1 && mX2 === mY2) return aX; // linear
      // Because JavaScript number are imprecise, we should guarantee the extremes are right.
      if (aX === 0) return 0;
      if (aX === 1) return 1;
      return calcBezier(getTForX(aX), mY1, mY2);
    };
    var str = "BezierEasing("+[mX1, mY1, mX2, mY2]+")";
    f.toString = function () { return str; };

    return f;
  }

  // CSS mapping
  BezierEasing.css = {
    "ease":        BezierEasing(0.25, 0.1, 0.25, 1.0),
    "linear":      BezierEasing(0.00, 0.0, 1.00, 1.0),
    "ease-in":     BezierEasing(0.42, 0.0, 1.00, 1.0),
    "ease-out":    BezierEasing(0.23, 1.25, 0.46, 1),
    "ease-in-out": BezierEasing(0.42, 0.0, 0.58, 1.0)
  };

  return BezierEasing;

}));



(function($) {
	var demo_id = 0;
	var page = 1;
	var ajaxurl = $("span.ajax-url-span").html();
	var calling = false;
	var svg;
	var s;
	var cur_step = 1;
	var max_steps = 6;
	var letters = 6;
	var stopTour = false;

	$(document).ready(function ()
	{	
		checkExtraLinks();

		if($("#bamboo").length){
			svg = document.getElementById("bamboo");
        	s = Snap(svg);

			$( "a.navbar-brand" ).hover(
			  function() {
			  	stopTour = false;
	    		startAnimation();
			  }, function() {
			    
			  }
			);
        }

        if($("#col-check-height").length){

			var win_h = $(window).height()- 60;
  	 		var col_h = $("#col-check-height").height();
  	 		var scroll_col_h = $(".scroll-on-fixed .col-container").height();

  	 		if((col_h > win_h) && (scroll_col_h < col_h)){
  	 			checkScrollCol();

  	 			window.addEventListener("scroll", function(event) {

	        		checkScrollCol();
				});
  	 		}
        		
        }

		checkJumboTitle();

		$( '.toggle-nav' ).on( 'click', function(e) {
			e.preventDefault();
			e.stopPropagation();
		 	toggleNav();

		 	 
		 	return false;
        });


        $( '.page-template-about .list-links a' ).on( 'click', function(e) {
        	var href = $(this).attr("href");

        	if(/^#/.test(href)){
        		e.preventDefault();

        		var top = $(href).offset().top;


				$('html, body').animate({
		            'scrollTop': top
		        }, 500, 'swing', function () {
		        });

        		return false;
        	}
        });

		$(".imgLiquidFill").imgLiquid();
		$('p:empty').remove();
		

		checkNewsletter();

		$(document).on('click', 'body .news-masonry .pagination a',function(e){
			e.preventDefault();


			var not_id = $("#last-news").attr("data-id");
			if(!$(this).hasClass("disabled")&&(!calling)){
				calling = true;
				var category = $(".filter-container .disabled").attr("data-term");

				if(category == '' || category == 'undefined'){
					category = 0;
				}

				page = $(this).attr("data-paged");
				var post_data;

				post_data = {
						page: page,
						category: category,
						not_id: not_id
				};

				var data = {
						action: "load_news",
						data: JSON.parse(JSON.stringify(post_data))
				};

				$.post(ajaxurl, data, function(response) {
					
					calling = false;
					if($(".news-archive-list").html(response)){
						$(".imgLiquidFill").imgLiquid();
						goToTopList();
					}
					else{
						console.log("error");
					}
				});
			}

			return false;
		});

		$(document).on('click', 'body .events-masonry .pagination a',function(e){
			e.preventDefault();

			if(!$(this).hasClass("disabled")&&(!calling)){
				calling = true;

				page = $(this).attr("data-paged");
				var post_data;

				post_data = {
						page: page
				};

				var data = {
						action: "load_events_by_ajax",
						data: JSON.parse(JSON.stringify(post_data))
				};


				$.post(ajaxurl, data, function(response) {
					
					calling = false;
					if($(".news-archive-list").html(response)){
						$(".imgLiquidFill").imgLiquid();
						goToTopList();
					}
					else{
						console.log("error");
					}
				});
			}

			return false;
		});

		$(document).on('click', 'body .technicals-container .pagination a',function(e){
			e.preventDefault();

			if(!$(this).hasClass("disabled")&&(!calling)){
				calling = true;
				var category = $(".filter-container .disabled").attr("data-term");

				if(category == '' || category == 'undefined'){
					category = 0;
				}

				page = $(this).attr("data-paged");
				var post_data;

				post_data = {
						page: page,
						category: category
				};

				console.log(post_data);

				var data = {
						action: "load_technicals",
						data: JSON.parse(JSON.stringify(post_data))
				};



				$.post(ajaxurl, data, function(response) {
					console.log(response);
					calling = false;
					if($(".col-list").html(response)){

						goToTopList();
					}
					else{
						console.log("error");
					}
				});
			}

			return false;
		});

		$(document).on('click', 'body .technicals-container .filter-container button',function(e){

				if(!$(this).hasClass("disabled")&&(!calling)){
					calling = true;
					var category = $(this).attr("data-term");
					$(".filter-container .disabled").removeClass("disabled");
					page = $(this).attr("data-paged");
					$(this).addClass("disabled");
					$(this).blur();

					page = 1;
					var post_data;

					post_data = {
							page: page,
							category: category
					};

					console.log(post_data);

					var data = {
							action: "load_technicals",
							data: JSON.parse(JSON.stringify(post_data))
					};


					$.post(ajaxurl, data, function(response) {
						console.log(response);
						calling = false;
						if($(".col-list").html(response)){
							goToTopList();
						}
						else{
							console.log("error");
						}
					});
				}

				return false;
		});

		if($(".demos-section").length){
        	checkDemos();
        }

        if($(".fake-row").length){
        	checkFakeTitle();
        }
	});

	 $(window).on('beforeunload', function(e) {
	     $('body').addClass('loading');

  	})

	 function removeLoading(){

	 	if($(".hp-content").length){
	 		var tl = new TimelineLite();
	 		var row_l = $(window).width();

	 		var row_container = $(".fake-h1");
	 		var end_w = row_container.width();


	 		var row_1 = $(".fake-row#row-1");
	 		var row_2 = $(".fake-row#row-2");
	 		var row_3 = $(".fake-row#row-3");
	 		var row_4 = $(".fake-row#row-4");
	 		var row_5 = $(".fake-row#row-5");
	 		var row_6 = $(".fake-row#row-6");

	 		tl.set(row_1, {left:"-200%"});
	 		tl.set(row_2, {left:"-200%"});
	 		tl.set(row_3, {left:"300%"});
	 		tl.set(row_4, {left:"300%"});
	 		tl.set(row_5, {left:"-200%"});
	 		tl.set(row_6, {left:"-200%"});

	 		var cont_w = $(".hp-content .container-fluid").width();

	 		tl.set(row_container, {width:cont_w});
	 		var txt_right = $(".hp-content .col-content .text-content");
	 		tl.set(txt_right, {top:"100%"});

	 		var w_80 = cont_w * 0.8;
	 		var w_60 = cont_w * 0.6;

	 		tl.add("step1", 0.5);
	 		tl.to(row_1, 0.5, {ease:new Ease(BezierEasing(0.23, 1.25, 0.46, 1)), left:30, width:w_80, onUpdate: checkFakeRow},"step1");
	 		tl.to(row_2, 0.5, {ease:new Ease(BezierEasing(0.23, 1.25, 0.46, 1)),  left:30, width:w_60, onUpdate: checkFakeRow}, "step1+=0.05");
	 		tl.to(row_3, 0.5, {ease:new Ease(BezierEasing(0.23, 1.25, 0.46, 1)), left:'10%', right:'10%', width:w_80, onUpdate: checkFakeRow},"step1");
	 		tl.to(row_4, 0.5, {ease:new Ease(BezierEasing(0.23, 1.25, 0.46, 1)), left:'20%', right:'20%', width:w_60, onUpdate: checkFakeRow},"step1");
	 		tl.to(row_5, 0.5, {ease:new Ease(BezierEasing(0.23, 1.25, 0.46, 1)), left:'auto', right:30, width:w_60, onUpdate: checkFakeRow},"step1+=0.45");
	 		tl.to(row_6, 0.5, {ease:new Ease(BezierEasing(0.23, 1.25, 0.46, 1)), left:'auto', right:30, width:'auto', onUpdate: checkFakeRow},"step1+=0.5");
	 		
	 		tl.add("step2", 2);
	 		tl.to(row_1, 0.25, {ease:Back.easeOut.config(0.5),  width:end_w, onUpdate: checkFakeRow},"step2");
	 		tl.to(row_2, 0.25, {ease:Back.easeOut.config(0.5), width:end_w, onUpdate: checkFakeRow},"step2+=0.05");
	 		tl.to(row_3, 0.25, {ease:Back.easeOut.config(0.5), width:end_w, left:30,onUpdate: checkFakeRow},"step2+=0.1");
	 		tl.to(row_4, 0.25, {ease:Back.easeOut.config(0.5), width:end_w,left:30, onUpdate: checkFakeRow},"step2+=0.15");
	 		tl.to(row_5, 0.25, {ease:Back.easeOut.config(0.5),  width:end_w, left:30,onUpdate: checkFakeRow},"step2+=0.2");
	 		tl.to(row_6, 0.25, {ease:Back.easeOut.config(0.5),left:30, onUpdate: checkFakeRow},"step2+=0.25");
	 	
	 		tl.add("step3", 2.7);
	 		end_w = $(".hp-content .text-container .title-content").width();
	 		tl.set(row_container, {width:end_w});


	 		tl.set(txt_right, {top:'initial'});


	 		var txt_main = $(".hp-content .col-content .main-content");
	 		var txt_links = $(".hp-content .col-content .list-links");
	 		var txt_footer = $(".hp-content .col-content #footer");

	 		tl.from(txt_main, 0.25, {ease: Power2.easeInOut, opacity:0, y:500},"step3");
	 		tl.from(txt_links, 0.25, {ease: Power2.easeInOut, opacity:0, y:500},"step3+=0.125");
	 		tl.from(txt_footer, 0.25, {ease: Power2.easeInOut, opacity:0, y:500},"step3+=0.25");
	 	}


      $('body').removeClass('loading');
  	 };

  	 function checkFakeRowSingle(row_name){

  	 	var row = $(row_name);
		var this_w = row.width();


	    var total_w = 0;

		  row.find(" svg" ).each(function() {
		  	var cur_id = row.attr("id");

		  	if(cur_id != 'var'){

		  		total_w += row.width();
		  	}
		  });

		 var diff = this_w - total_w;

		  if(diff > 0){
		  	 var viewBox = "0 0 " + diff + " 112";
			  row.find("svg#var").width(diff);
			  row.find("svg#var").attr("viewBox",viewBox);
		  }
	}

  	 function checkScrollCol(){
  	 	var y = getScrollY();
  	 	var head_h = 90;
  	 	var col = document.getElementById('col-check-height');
  	 	var top = col.offsetTop - head_h;
  	 	var col_h = $("#col-check-height").height();
  	 	var scroll_col = $(".scroll-on-fixed .col-container");
  	 	var scroll_col_h = scroll_col.height();
  	 	var diff_h = col_h - scroll_col_h;

  	 	if(y > top){
  	 		if(!scroll_col.hasClass("is-fixed")){
  	 			scroll_col.addClass("is-fixed")
  	 		}


  	 		if((y - top) > diff_h)
  	 		{
  	 			var scroll_diff = y - top - diff_h;
  	 			var top_fixed = head_h - scroll_diff;
  	 			scroll_col.css("top",top_fixed);

  	 		}
  	 		else
  	 		{
  	 			scroll_col.css("top",head_h);
  	 		}	
  	 	}
  	 	else
  	 	{
  	 		if(scroll_col.hasClass("is-fixed")){
  	 			scroll_col.removeClass("is-fixed")
  	 		}
  	 	}
  	 }


  	 

  	 function getScrollY() {
	    var  scrOfY = 0;
	    if( typeof( window.pageYOffset ) == 'number' ) {
	        //Netscape compliant
	        scrOfY = window.pageYOffset;

	    } else if( document.body && document.body.scrollTop )  {
	        //DOM compliant
	        scrOfY = document.body.scrollTop;
	    } 
	    return scrOfY;
	}

	$(window).on('load', function() {
		
		if($("#col-check-height").length){

			var win_h = $(window).height()- 60;
  	 		var col_h = $("#col-check-height").height();
  	 		var scroll_col_h = $(".scroll-on-fixed .col-container").height();

  	 		if((col_h > win_h) && (scroll_col_h < col_h)){
  	 			checkScrollCol();

  	 			window.addEventListener("scroll", function(event) {

	        		checkScrollCol();
				});
  	 		}
        		
        }

		window.setTimeout( removeLoading, 1000 ); // 5 seconds

		checkNewsletter();
		checkJumboTitle();
		checkExtraLinks();

		if($(".demos-section").length){
        	checkDemos();
        }

        if($(".fake-row").length){
        	checkFakeTitle();
        }
	});

	$( window ).on('resize', function() {
		checkNewsletter();
		checkJumboTitle();
		checkExtraLinks();

		if($(".demos-section").length){
        	checkDemos();
        }

        if($(".fake-row").length){
        	checkFakeTitle();
        }

        if($("#col-check-height").length){

			var win_h = $(window).height()- 60;
  	 		var col_h = $("#col-check-height").height();
  	 		var scroll_col_h = $(".scroll-on-fixed .col-container").height();

  	 		if((col_h > win_h) && (scroll_col_h < col_h)){
  	 			checkScrollCol();

  	 			window.addEventListener("scroll", function(event) {

	        		checkScrollCol();
				});
  	 		}
        }
	});

	function startAnimation(){

		if((cur_step == 1) && (stopTour))
		{
			
		}
		else
		{
			var current_step = cur_step;
			var next_step = current_step + 1;

			if(current_step == max_steps){
				next_step = 1;
			}

			var i;
			for (i = 1; i <= letters; i++) { 
				var cur_name = "#step-0-letter-"+i;
			    var cur_letter = Snap.select(cur_name);

			    var next_name = "#step-"+next_step+"-letter-"+i;
			    var next_letter = Snap.select(next_name);
			    var next_d = next_letter.node.getAttribute('d');

			    if(i != letters){
			    	cur_letter.animate({ d: next_d }, 1000, mina.easeinout); 
			    }
			    else
			    {
			    	if(cur_step == max_steps){
						cur_step = 1;
						stopTour = true;
					}
					else
					{
						cur_step += 1;
						
					}

					cur_letter.animate({ d: next_d }, 1000, mina.easeinout, startAnimation); 
			    }
			}
		}
	}

	function checkJumboTitle(){

		var h2 = $('.jumbo-container .text-container h2').height();
		var mrgntp = parseInt($('.jumbo-container .text-container h2').css("margin-top"));
		var h1 = parseInt($('.jumbo-container .text-container h1').css("line-height"));

		var total = h2 + 10;

		if(h1 > 100)
		{
			total = h2 + mrgntp + h1;
		}

		$('.jumbo-container .text-container .arrow-line').height(total);
	}

	function checkDemos(){
		$( ".demo-link " ).each(function() {
		  
		  var img_h = $(this).find(".text-container .icon").height();
		  var txt_h = $(this).find(".text-container .info-container").height();
		  var cur_h = img_h + txt_h + 28;
		  var cont_h = $(this).find(".outer-container").height() - 30;
		  //var mrgntp = (cur_h / 2) * -1;
		  var mrgntp = ((cont_h - cur_h) / 2);
		  $(this).find(".text-container").css("margin-top",mrgntp);
		  

		});
	}

	function checkFakeTitle(){

		var max_h = $(window).height() - 60 - parseInt($( ".fake-h1").css("padding-top")) - parseInt($( ".fake-h1").css("padding-bottom")) * 2;
		var row_h = parseInt((max_h - 65) / 6);
		
		if(row_h > 112)
		{
			row_h = 112;
		}

		$( ".fake-h1 .fake-row" ).css("height",row_h);
		$( ".fake-h1 .fake-row svg" ).css("height",row_h);

		checkFakeRow();
	}

	

	function checkFakeRow(){

		var counter = 0;
		var reverse_counter = 5;
		var start_h = parseInt($( ".fake-h1").css("padding-top"));
		var top = 0;
		var cur_h = 0;

		$( ".fake-h1 .fake-row" ).each(function() {
		  var this_w = $(this).width();
		  var total_w = 0;

		  $( this).find(" svg" ).each(function() {
		  	var cur_id = $(this).attr("id");

		  	if(cur_id != 'var'){

		  		total_w += $(this).width();
		  	}
		  });

		  cur_h =  $(this).height()  + 20;

		  top = start_h + cur_h * counter;
		  var bottom = start_h + cur_h * (reverse_counter - counter);

		  $(this).css("bottom",bottom);

		  var diff = this_w - total_w;

		  if(diff > 0){
		  	 var viewBox = "0 0 " + diff + " 112";
			  $(this).find("svg#var").width(diff);
			  $(this).find("svg#var").attr("viewBox",viewBox);
		  }
		 	
		  counter++;
		});

		var fake_h = cur_h + top;

		$( ".fake-h1").height(fake_h);
	}

	function goToTopList(){

		if($(".news-archive-list").length){
			var top = $(".news-archive-list").offset().top - 60;

			$('html, body').stop().animate({
	            'scrollTop': top
	        }, 500, 'swing', function () {
	        });
		}
		else
		if($(".col-list").length){
			var top = $(".col-list").offset().top - 60;

			$('html, body').stop().animate({
	            'scrollTop': top
	        }, 500, 'swing', function () {
	        });
		}

        
	}

	function checkNewsletter(){
		if($(".newsletter-footer").length){
			var content_h = $(".newsletter-footer .text-container").height();
			$(".newsletter-footer").height(content_h);
			$(".newsletter-footer .newsletter-content").height(content_h);
		}
	}

	function checkExtraLinks(){

		if($(".inner-menu-wrapper .extra-links").length){
			var ul_h = $("ul.navbar-nav").outerHeight();
			var extra_h = $(".inner-menu-wrapper .extra-links").height();
			var inner_h = $(".inner-menu-wrapper").height();

			if((extra_h + ul_h) > inner_h){
				if(!$(".inner-menu-wrapper .extra-links").hasClass("relative")){
					$(".inner-menu-wrapper .extra-links").addClass("relative");
				}
			}
			else
			{
				if($(".inner-menu-wrapper .extra-links").hasClass("relative")){
					$(".inner-menu-wrapper .extra-links").removeClass("relative");
				}
			}
		}

		
	}
	

	function toggleNav(event) {
		var wrapper = $('#site-wrapper');
		var menu = $('nav.navbar');
		var ul_h = $("ul.navbar-nav").outerHeight();
		var extra_h = $(".inner-menu-wrapper .extra-links").height();
		var inner = $("ul.navbar-nav");
		var inner_h = $(".inner-menu-wrapper").height();
		var max_scroll = inner_h - ul_h - extra_h;

		
	    if (wrapper.hasClass('show-nav')) {
	       
	       document.getElementById("site-canvas").removeEventListener("click", toggleNav);
	        wrapper.removeClass('show-nav');
	        $("body").removeClass("opening");

	        menu.off('mousewheel DOMMouseScroll', function(event) {
		    });

		    wrapper.stop().animate({
			    top: 0,
			  }, 100, function() {
			    // Animation complete.
			  });
	    } else {
	        
	        document.getElementById("site-canvas").addEventListener("click", toggleNav);
	        wrapper.addClass('show-nav');
	        $("body").addClass("opening");

	        if((extra_h + ul_h) > inner_h){

	        	menu.on('mousewheel DOMMouseScroll', function(e){

				  var dat = $(e.delegateTarget).data(); //in case you have set some, read it here.
				  var datx = dat.x || 0; // just to show how to get specific out of jq-data object

				  var eo = e.originalEvent;
				  var xy = eo.wheelDelta || -eo.detail; //shortest possible code
				  var x = eo.wheelDeltaX || (eo.axis==1?xy:0);
				  var y = eo.wheelDeltaY || (eo.axis==2?xy:0); // () necessary!
				  var top = parseInt(inner.css("margin-top"), 10);
				  var delta = 30;

				  if (y > 0) {
						top += (xy * delta);

						if(top > 0)
						{
							top = 0;
						}
					}
					else
					{
						top += (xy * delta);

						if(top < max_scroll){
							top = max_scroll;
						}
					}

					inner.stop().animate({
					    'margin-top': top,
					  }, 50, function() {
					    // Animation complete.
					  });

				});



				/*menu.on('mousewheel DOMMouseScroll', function(e) {

					

					console.log("delta = "+e.xy);
					console.log("y = "+e.y);
					if (e.deltaY > 0) {
						if (e.type == 'mousewheel') {
					        top -= (e.originalEvent.wheelDelta * 30);
					    }
					    else if (e.type == 'DOMMouseScroll') {
					        top -= (e.originalEvent.detail * 30);
					    }

						if(top < max_scroll){
							top = max_scroll;
						}
					}
					else
					{

						if (e.type == 'mousewheel') {
					        top += (e.originalEvent.wheelDelta * 30);
					    }
					    else if (e.type == 'DOMMouseScroll') {
					        top += (e.originalEvent.detail * 30);
					    }

						if(top > 0)
						{
							top = 0;
						}
					}

					console.log("top = "+top);

					inner.stop().animate({
					    top: top,
					  }, 100, function() {
					    // Animation complete.
					  });
				});*/
			}
	    }
	}

})(jQuery);
