<?php
/**
 * Default Events Template
 * This file is the basic wrapper template for all the views if 'Default Events Template'
 * is selected in Events -> Settings -> Template -> Events Template.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/default-template.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$context = Timber::get_context();

if(is_archive()){
	$context['posts'] = new Timber\PostQuery();

	$context['events_archive'] = is_archive();
	

	$context['events_category'] = Timber::get_terms( 'tribe_events_cat', array(
    'hide_empty' => true,
	) );


	$debug = '';
	$cur_page = 1;
	$per_page = 12; // POI 12

	$is_past = tribe_is_past();

	if(!$is_past){

		$per_page = 3;
	}

	$args = array(
		'post_type' => 'tribe_events',
		'posts_per_page' => $per_page,
		'paged'          => $cur_page,
		'post_status'=>'publish',
		'eventDisplay' => 'past',
		'meta_key'=>'_EventStartDate',
	  'orderby'=>'_EventStartDate',
	  'order'=>'DESC'

	);


	$custom_query = new WP_Query($args);
	$total = $custom_query->found_posts;
	$total_pages = $custom_query->max_num_pages;

	$msg = '';

	if ( $custom_query->have_posts() ){

			//$msg .= '<div class="past-events col-list">';
			//$msg .= '<div class="row">';

		  while ( $custom_query->have_posts() ){
					$custom_query->the_post();
		      //setup_postdata( $post );
		      $size = 'medium';
		      $cur_id = get_the_ID();

		      $msg .= '<div class="col-xl-3 col-lg-4 col-md-6 col-preview">';
		      $msg .= '<a class="article-preview-medium" href="'.get_the_permalink($cur_id ).'">';
		      $msg .= '<div class="image-wrapper ">';
			  $msg .= '<div class="image-container imgLiquid imgLiquidFill"  data-imgLiquid-fill="true" data-imgLiquid-horizontalAlign="center" data-imgLiquid-verticalAlign="50%">';
		      $msg .= '<img  src="'.get_the_post_thumbnail_url($cur_id,'large').'" />';
		      $msg .= '</div><!-- image-container -->';
		      $msg .= '</div><!-- image-wrapper -->';
		      $msg .= '<h3 class="standard-title">'.get_the_title($cur_id).'</h3>';
		       $msg .= '<p>';
		      $msg .= '<b>';

			  $event = get_post( $cur_id);

				$msg .= getEventVenuePreview($cur_id).' - '.getEventDatePreview($event);
			   $msg .= '</b><br/>';


		      $msg .= get_the_excerpt( $cur_id );
		      $msg .= '</p>';
		       $msg .= '<span  class="preview-link ">';
		      $msg .= get_field("learn_more_label","options");
		      $msg .= '</span>';
		      $msg .= '</a><!-- article-preview -->';
		      $msg .= '</div><!-- col-preview -->';

		  }

		  if($is_past){
		  	$page = get_field("page_label","options");
		    $first = get_field("first_label","options");
		    $previous = get_field("prev_label","options");
		    $next = get_field("next_label","options");
		    $last = get_field("last_label","options");
		    $of = get_field("of_label","options");

			  if(($total >= $per_page) && ($total_pages > 1))
			  {
			      $msg .= '<div class="pagination text-center ">';

			      $msg .= '<span class="d-block d-md-none">'.$page.' '.$cur_page.' of '.$total_pages.'</span>';
			      $class = '';

			      if($cur_page == 1)
			        $class = 'disabled';

			      $msg .= '<a class="'.$class.'" href="#" dat-paged="1" >'.$first.'</a>';
			      $msg .= '<a class="'.$class.'" href="#" data-paged="'.($cur_page - 1).'" >←'.$previous.'</a>';
			      $msg .= '<span class="d-none d-md-block d-lg-block">'.$page.' '.$cur_page.' '.$of.' '.$total_pages.'</span>';
			      $class = '';

			      if($cur_page == $total_pages)
			        $class = 'disabled';

			      $msg .= '<a class="'.$class.'" href="#" data-paged="'.($cur_page + 1).'" >'.$next.'→</a>';
			      $msg .= '<a class="'.$class.'" href="#" data-paged="'.$total_pages.'" >'.$last .'</a>';
			      $msg .= '</div><!-- pagination --> ';
			  }
		  }
		}

	$context['past_events'] = $msg;
	$context['is_past'] = $is_past;
	$content['past_link'] = tribe_get_past_link();

	$context['adminurl'] = admin_url('admin-ajax.php');
	Timber::render('archive-tribe_events.twig', $context);
}
else {
	$post = new TimberPost();
	$context['post'] = $post;

	$context["is_single_event"] = true;


	$url =  "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
	$escaped_url = urlencode(htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' ));

	$fb_url = 'https://www.facebook.com/sharer/sharer.php?u='.$escaped_url;

	$cur_title = urlencode(get_the_title());
	$linkedin_url = 'https://www.linkedin.com/shareArticle?mini=true&url='.$escaped_url.'&title='.$cur_title;

	//$twitter_url = 'https://twitter.com/home?status='.$escaped_url;
	//$twitter_params = urlencode(get_the_title() . " ". htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' ) . ' via @stardusth2020');
	//$twitter_url = 'https://twitter.com/home?status='.$twitter_params;
	$twitter_url = 'https://twitter.com/intent/tweet?url='.$escaped_url.'&via=bambooH2020&text='.$cur_title;

	
	$context['fb_url'] = $fb_url;
	$context['linkedin_url'] = $linkedin_url;
	$context['twitter_url'] = $twitter_url;

	$event_id = $post->id;
	$event = get_post($event_id);

	$start_date =  new DateTime($event->EventStartDate);
	$end_date =  new DateTime($event->EventEndDate);

	$start_time =  strtotime($event->EventStartDate);
	$end_time = strtotime($event->EventEndDate);
	$start_minutes = date('i',$start_time);
	$end_minutes = date('i',$end_time);


	$start_date_no_time = $start_date->format('dmY');
	$end_date_no_time = $end_date->format('dmY');
	$start_day = $start_date->format('jS');
	$start_week_day = $start_date->format('D');
	$start_month = $start_date->format('F');
	$start_year = $start_date->format('Y');
	$start_time = $start_date->format('H') . ":" . $start_minutes;
	$end_day = $end_date->format('jS');
	$end_week_day = $end_date->format('D');
	$end_month = $end_date->format('F');
	$end_year = $end_date->format('Y');
	$end_time = $end_date->format('H') . ":" . $end_minutes;


	$the_single = '';

	if($start_date_no_time != $end_date_no_time)
	{
		$the_single = 'from '.$start_month . " ".$start_day. ', '.$start_year;

		if(!tribe_event_is_all_day($event_id))
		{
			$the_single .= ' at '.$start_time;
		}

		$the_single .= '<br/>to '.$end_month.' '.$end_day. ', '.$end_year;

		if(!tribe_event_is_all_day($event_id))
		{
			$the_single .= ' at '.$end_time;
		}
	}
	else {
		$the_single = $start_month . " ".$start_day. ", ".$start_year;

		if(!tribe_event_is_all_day($event_id))
		{
			$the_single .= ','.$start_time.'-'.$end_time;
		}
	}


 	$context['getEventDateSingle'] = $the_single;
	$context['getEventDate'] =  getEventDatePreview($event);

	$venue_string = "";

	$venue = get_field ("venue",$event_id);
	$address = get_field ("address",$event_id);
	$city = get_field ("city",$event_id);
	$country = get_field ("country",$event_id);

	if($venue || $city  || $country){

		if($venue){
			$venue_string = $venue;

			if($city || $country || $address){
				$venue_string .= "<br/>";
			}

			if($address)
			{
				$venue_string .= $address;

				if($city || $country)
				{
					$venue_string .= " - ";
				}
			}

			if($city && $country){
				$venue_string .= $city.", ".$country;
			}
			else
			if($city){
				$venue_string .= $city;
			}
			else
			if($country){
				$venue_string .= $country;
			}
			
		}
		else
		{
			if($city && $country){
				$venue_string = $city.", ".$country;
			}
			else
			if($city){
				$venue_string = $city;
			}
			else
			if($country){
				$venue_string = $country;
			}
		}

		$venue_string.='<br/>';
	}
	else{
		$venue = tribe_get_venue_details($event_id)['linked_name'];
		$address = tribe_get_venue_details($event_id)['address'];

		if($venue){
			$venue_string .= $venue.'<br/>';
			$venue_string .= $address.'<br/>';
		}
	}
 


	$context['getEventLocationSingle'] =  $venue_string;
	$context['getEventLocation'] =  getEventVenuePreview($event_id);
	//$context['map'] =  tribe_get_embedded_map($event_id);
	$context['iCal'] =  tribe_get_ical_link($event_id);
	$context['google_calendar'] =  tribe_get_gcal_link($event_id);

	Timber::render( 'single-event.twig', $context );
}
