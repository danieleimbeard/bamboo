<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 * @version  4.3
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;



$url =  "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
$escaped_url = urlencode(htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' ));

$fb_url = 'https://www.facebook.com/sharer/sharer.php?u='.$escaped_url;

$cur_title = urlencode(get_the_title());
$linkedin_url = 'https://www.linkedin.com/shareArticle?mini=true&url='.$escaped_url.'&title='.$cur_title;

//$twitter_url = 'https://twitter.com/home?status='.$escaped_url;
//$twitter_params = urlencode(get_the_title() . " ". htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' ) . ' via @stardusth2020');
//$twitter_url = 'https://twitter.com/home?status='.$twitter_params;
$twitter_url = 'https://twitter.com/intent/tweet?url='.$escaped_url.'&via=bambooH2020&text='.$cur_title;

$mail_url = 'mailto:?subject='.get_field("share_mail_subject","options").'&body='.get_field("share_mail_text","options"). ' '. $escaped_url;
$prova = $post->ID;
$context['fb_url'] = $fb_url;
$context['linkedin_url'] = $linkedin_url;
$context['twitter_url'] = $twitter_url;
$context['mail_url'] = $mail_url;
Timber::render( 'single-event.twig', $context );
