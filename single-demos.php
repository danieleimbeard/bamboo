<?php
/*
 * Template Name: SINGLE DEMOS
 */




$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

$context["is_single_demos"] = true;

$context["single_demos_class"] = 'single-demos-jumbo';
$context['demos_archive_link'] = get_permalink(11);

Timber::render( 'single-demos.twig', $context );

